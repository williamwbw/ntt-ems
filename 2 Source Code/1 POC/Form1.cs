﻿using NTT_POC_V2.RT_SV1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NTT_POC_V2
{
    public partial class Form1 : Form
    {
        string SessionID = string.Empty;
        string UserName = string.Empty;
        string Password = string.Empty;
        string SelectedNode = string.Empty;

        RT_SV1.RealTimeData RTD;
        SC_SV1.SessionContext svSession;

        public Form1()
        {
            InitializeComponent();
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender1, cert, chain, sslPolicyErrors) => true;
        }          

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            RTD = new RT_SV1.RealTimeData();
            RTD.Url = "https://" + IP.Text + "/RealtimeData/Realtimedata.asmx";
            UserName = this.txtUserName.Text;
            Password = this.txtPassword.Text;
            SC_SV1.SVLanguage sV = SC_SV1.SVLanguage.ContextDefault;
            svSession = new SC_SV1.SessionContext();
            SC_SV1.Result result = svSession.OpenSession(UserName, Password, sV, out SessionID);

            this.txtSessionID.Text = SessionID;

            RT_SV1.VariableCollectionIterator iterator = new RT_SV1.VariableCollectionIterator();
            RT_SV1.VariableCollectionRecord record = new RT_SV1.VariableCollectionRecord();
            var test = RTD.Browse(SessionID, iterator, out record);

            List<VariableCollection> lstRecord = record.variableCollections.ToList();

            for(int i = 0; i < lstRecord.Count; ++i)
            {
                string root = string.Empty;
                foreach (var bran in lstRecord[i].branches)
                {
                    if (!bran.Equals(""))
                    {
                        root += bran + '.';
                    }
                }
                tvBrowser.Nodes.Add(root + lstRecord[i].VariableName);
            }
        }

        private void tvBrowser_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SelectedNode = e.Node.Text;
            RT_SV1.VariableRequestParameters variableRequest = new RT_SV1.VariableRequestParameters()
            {
                variableNames = new string[] { e.Node.Text },
                variableProperties = new RT_SV1.VariableProperty[] { RT_SV1.VariableProperty.VariableName }
            };
            VariableProperties[] variableProperties;
            RTD.GetProperties(SessionID, variableRequest, out variableProperties);

            //string[] selectedvariable = e.Node.Text.Split('=');
            //variableRequest.variableNames = new string [] { selectedvariable[0] };
            //variableRequest.variableProperties = new RT_SV1.VariableProperty[] { RT_SV1.VariableProperty.VariableName };
            RT_SV1.VariableValue[] responseValues;

            RT_SV1.Result svResult = RTD.Read(SessionID, variableRequest, out responseValues);
            string strValues = string.Empty;
            foreach(var value in responseValues)
            {
                strValues += value.value.ToString();
            }

            SelectedValue.Text = strValues;
        }

        private void Write_Click(object sender, EventArgs e)
        {
            RT_SV1.UpdateValue updateValue = new RT_SV1.UpdateValue();
            updateValue.Name = SelectedNode;
            updateValue.Value = txtWrite.Text;
            RT_SV1.UpdateValuesParameters updateValuesParameters = new RT_SV1.UpdateValuesParameters();
            updateValuesParameters.UpdateValues = new UpdateValue[] { updateValue };

            RT_SV1.Result[] result;
            RT_SV1.Result svResult = RTD.UpdateValues(SessionID, updateValuesParameters, out result);
        }

        private void btnDC_Click(object sender, EventArgs e)
        {
            svSession.CloseSession(SessionID);
            SelectedValue.Text = string.Empty;
            IP.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtUserName.Text = string.Empty;
            txtSessionID.Text = string.Empty;
            tvBrowser.Nodes.Clear();
        }
    }
}
