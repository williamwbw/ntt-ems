﻿using Newtonsoft.Json;
using NTT_EMS_ControlPanel.Modal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NTT_EMS_ControlPanel
{
    public partial class PCVUEDataConfiguration : Form
    {
        string configurationPath = Environment.CurrentDirectory + "\\Configuration";
        string fileName = "PCVUE.json";
        public PCVUEDataConfiguration()
        {
            InitializeComponent();
            AddPCVueServer();
        }

        public void AddPCVueServer()
        {
            var jsonData = File.ReadAllText(configurationPath + "\\" + fileName);
            var PCVUEList = JsonConvert.DeserializeObject<List<PCVUEData>>(jsonData);
            var count = 0;
            foreach (var PCVUEData in PCVUEList)
            {
                this.ddPCVUE.Items.Insert(count, PCVUEData.ServerName);
                ++count;
            }
        }
    }
}
