﻿using Newtonsoft.Json;
using NTT_EMS_ControlPanel.Modal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NTT_EMS_ControlPanel
{
    public partial class PCVUE : Form
    {
        public PCVUE()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PCVUEData objPCVUE = new PCVUEData();
            objPCVUE.ServerName = txtServerName.Text;
            objPCVUE.ServerIP = txtServerIP.Text;
            objPCVUE.ClientID = txtClientIP.Text;
            objPCVUE.ClientSecret = txtClientSecret.Text;

            string configurationPath = Environment.CurrentDirectory + "\\Configuration";
            string fileName = "PCVUE.json";
            if(!Directory.Exists(configurationPath))
            {
                Directory.CreateDirectory(configurationPath);
                var jsonData = JsonConvert.SerializeObject(objPCVUE);
                System.IO.File.WriteAllText(configurationPath + "\\" + fileName, jsonData);
            }
            else
            {
                var jsonData = File.ReadAllText(configurationPath + "\\" + fileName);
                var PCVUEList = JsonConvert.DeserializeObject<List<PCVUEData>>(jsonData) ?? new List<PCVUEData>();
                PCVUEList.Add(objPCVUE);
                jsonData = JsonConvert.SerializeObject(PCVUEList);
                System.IO.File.WriteAllText(configurationPath + "\\" + fileName, jsonData);
            }
            var SavedData = File.ReadAllText(configurationPath + "\\" + fileName);
            rtbpcvue.Text = JsonConvert.SerializeObject(SavedData);

        }
    }
}
