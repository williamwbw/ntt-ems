﻿using Newtonsoft.Json;
using NTT_EMS_ControlPanel.Modal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NTT_EMS_ControlPanel
{
    public partial class HomeScreen : Form
    {
        string configurationPath = Environment.CurrentDirectory + "\\Configuration";
        string fileName = "PCVUE.json";

        public HomeScreen()
        {
            InitializeComponent();
            var jsonData = File.ReadAllText(configurationPath + "\\" + fileName);
            var PCVUEList = JsonConvert.DeserializeObject<List<PCVUEData>>(jsonData);
            dgvPCVUE.AutoGenerateColumns = true;
            var count = 0;
            foreach(var PCVUEData in PCVUEList)
            {
                ++count;
                this.dgvPCVUE.Rows.Add(count, "PCVUE" ,PCVUEData.ServerName, PCVUEData.ServerIP, PCVUEData.ClientID, PCVUEData.ClientSecret);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PCVUE pcvue = new PCVUE();
            pcvue.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MOSDataConfiguration modData = new MOSDataConfiguration();
            modData.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.dgvPCVUE.Rows.Clear();
            var jsonData = File.ReadAllText(configurationPath + "\\" + fileName);
            var PCVUEList = JsonConvert.DeserializeObject<List<PCVUEData>>(jsonData);
            dgvPCVUE.AutoGenerateColumns = true;
            var count = 0;
            foreach (var PCVUEData in PCVUEList)
            {
                ++count;
                this.dgvPCVUE.Rows.Add(count, "PCVUE", PCVUEData.ServerName, PCVUEData.ServerIP, PCVUEData.ClientID, PCVUEData.ClientSecret);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            PCVUEDataConfiguration pcvuedata = new PCVUEDataConfiguration();
            pcvuedata.Show();
        }
    }
}
